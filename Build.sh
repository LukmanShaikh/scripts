#!/bin/bash
# Copyright (C) 2015 Paranoid Android Project# Copyright (C) 2018 lukatthisman<shaikhlukman01@gmail.com
# Copyright (C) 2018 lukmanshaikh
 
# PA Colors
# red = errors, cyan = warnings, green = confirmations, blue = informational
# plain for generic text, bold for titles, reset flag at each end of line
# plain blue should not be used for readability reasons - use plain cyan instead
CLR_RST=$(tput sgr0)                        ## reset flag
CLR_RED=$CLR_RST$(tput setaf 1)             #  red, plain
CLR_GRN=$CLR_RST$(tput setaf 2)             #  green, plain
CLR_YLW=$CLR_RST$(tput setaf 3)             #  yellow, plain
CLR_BLU=$CLR_RST$(tput setaf 4)             #  blue, plain
CLR_PPL=$CLR_RST$(tput setaf 5)             #  purple,plain
CLR_CYA=$CLR_RST$(tput setaf 6)             #  cyan, plain
CLR_BLD=$(tput bold)                        ## bold flag
CLR_BLD_RED=$CLR_RST$CLR_BLD$(tput setaf 1) #  red, bold
CLR_BLD_GRN=$CLR_RST$CLR_BLD$(tput setaf 2) #  green, bold
CLR_BLD_YLW=$CLR_RST$CLR_BLD$(tput setaf 3) #  yellow, bold
CLR_BLD_BLU=$CLR_RST$CLR_BLD$(tput setaf 4) #  blue, bold
CLR_BLD_PPL=$CLR_RST$CLR_BLD$(tput setaf 5) #  purple, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
echo -e ""
echo -e "${CLR_BLD_GRN}##       ##     ## ##    ## ##     ##    ###    ##    ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##   ##  ###   ###   ## ##   ###   ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##  ##   #### ####  ##   ##  ####  ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## #####    ## ### ## ##     ## ## ## ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##  ##   ##     ## ######### ##  ####${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##   ##  ##     ## ##     ## ##   ###${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}########  #######  ##    ## ##     ## ##     ## ##    ##${CLR_BLD_GRN}" 
echo -e ""
printf "\033[1mWhich rom Are We Compiling Today?\033[0m\n"

printf "\033[1mEnter mdr - For mdroid rom\033[0m\n"

printf "\033[1mEnter lr-old - For liquid rom\033[0m\n"

printf "\033[1mEnter lr-new - For liquid-new rom\033[0m\n"

read var1

if [ $var1 = "mdr" ]

then

printf "\033[1;31mCompiling mdroid rom\033[0m\n"

repo sync --force-sync

rm -rf device/xiaomi/land

rm -rf kernel/xiaomi/msm8937

rm -rf vendor/xiaomi

git clone https://github.com/lukmanshaikh/mdroid_device_xiaomi_land.git -b stable device/xiaomi/land

git clone https://github.com/lukmanshaikh/kernel_xiaomi_msm8937.git -b stable kernel/xiaomi/msm8937

git clone https://github.com/lukmanshaikh/vendor_xiaomi_land.git -b stable vendor/xiaomi

git clone https://github.com/LineageOS/android_external_json-c.git -b lineage-15.1 external/json-c

git clone https://github.com/krasCGQ/aarch64-linux-android/tree/opt-gnu-8.x -b opt-gnu-8.x prebuilts/gcc/linux-x86/aarch64/gcc-8.1

rm -rf hardware/qcom/audio-caf

rm -rf hardware/qcom/media-caf

rm -rf hardware/qcom/display-caf

git clone https://github.com/TeamReloaded/hardware_qcom_audio.git -b lineage-15.1-caf-8996 hardware/qcom/audio-caf/msm8996

git clone https://github.com/TeamReloaded/hardware_qcom_display.git -b lineage-15.1-caf-8996 hardware/qcom/display-caf/msm8996

git clone https://github.com/TeamReloaded/hardware_qcom_media.git -b lineage-15.1-caf-8996 hardware/qcom/media-caf/msm8996

cd bionic && git fetch https://github.com/TeamReloaded/bionic && git cherry-pick 1e55ab48ba3f3db809163e0476051aad718d70f2^..678dd8ad22225dfef8d76a2e9cac92dc3a843e88 && cd ..

. build/envsetup.sh && lunch mdroid_land-userdebug && mka bacon

echo -e "${CLR_BLD_GRN}Build Complete...${CLR_RST}"

echo -e ""

echo -e "${CLR_BLD_GRN}Now You are good to Go${CLR_RST}"

echo -e ""

echo -e ""

elif [ $var1 = "lr-old" ]

then

printf "\e[1;32mCompiling lr-old\e[0m\n"

repo sync --force-sync

rm -rf ota_conf

wget https://github.com/lukmanshaikh/LR-OTA/raw/oc-mr2/ota_conf

rm -rf device/xiaomi/land

rm -rf kernel/xiaomi/msm8937

rm -rf vendor/xiaomi

git clone https://github.com/LiquidRemix-Devices/android_device_xiaomi_land.git -b oc-mr2 device/xiaomi/land

git clone https://github.com/lukmanshaikh/kernel_xiaomi_msm8937.git -b lineage-15.1 kernel/xiaomi/msm8937

git clone https://github.com/LiquidRemix-Devices/proprietary_vendor_xiaomi_land.git -b oc-mr2 vendor/xiaomi

rm -rf hardware/qcom/audio-caf

rm -rf hardware/qcom/media-caf

rm -rf hardware/qcom/display-caf

git clone https://github.com/TeamReloaded/hardware_qcom_audio.git -b lineage-15.1-caf-8996 hardware/qcom/audio-caf/msm8996

git clone https://github.com/TeamReloaded/hardware_qcom_display.git -b lineage-15.1-caf-8996 hardware/qcom/display-caf/msm8996

git clone https://github.com/TeamReloaded/hardware_qcom_media.git -b lineage-15.1-caf-8996 hardware/qcom/media-caf/msm8996

cd bionic && git fetch https://github.com/TeamReloaded/bionic && git cherry-pick 1e55ab48ba3f3db809163e0476051aad718d70f2^..678dd8ad22225dfef8d76a2e9cac92dc3a843e88 && cd ..

. build/envsetup.sh && lunch liquid_land-userdebug && brunch land

echo -e "${CLR_BLD_GRN}Build Complete...${CLR_RST}"

echo -e ""

echo -e "${CLR_BLD_GRN}Now You are good to Go${CLR_RST}"

echo -e ""

echo -e ""

elif [ $var1 = "lr-new" ]

then

printf "\e[1;32mCompiling Lr-new\e[0m\n"

repo sync --force-sync

rm -rf ota_conf

wget https://github.com/lukmanshaikh/LR-OTA/raw/oc-mr2/ota_conf

rm -rf device/xiaomi/land

rm -rf kernel/xiaomi/msm8937

rm -rf vendor/xiaomi

git clone https://github.com/LiquidRemix-Devices/android_device_xiaomi_land.git -b oc-mr1 device/xiaomi/land

git clone https://github.com/lukmanshaikh/kernel_xiaomi_msm8937.git -b lineage-15.1 kernel/xiaomi/msm8937

git clone https://github.com/LiquidRemix-Devices/proprietary_vendor_xiaomi_land.git -b oc-mr1 vendor/xiaomi

rm -rf hardware/qcom/audio-caf

rm -rf hardware/qcom/media-caf

rm -rf hardware/qcom/display-caf

git clone https://github.com/TeamReloaded/hardware_qcom_audio.git -b lineage-15.1-caf-8996 hardware/qcom/audio-caf/msm8996

git clone https://github.com/TeamReloaded/hardware_qcom_display.git -b lineage-15.1-caf-8996 hardware/qcom/display-caf/msm8996

git clone https://github.com/TeamReloaded/hardware_qcom_media.git -b lineage-15.1-caf-8996 hardware/qcom/media-caf/msm8996

cd bionic && git fetch https://github.com/TeamReloaded/bionic && git cherry-pick 1e55ab48ba3f3db809163e0476051aad718d70f2^..678dd8ad22225dfef8d76a2e9cac92dc3a843e88 && cd ..

. build/envsetup.sh && lunch liquid_land-userdebug && brunch land

echo -e "${CLR_BLD_GRN}Build Complete...${CLR_RST}"

echo -e ""

echo -e "${CLR_BLD_GRN}Now You are good to Go${CLR_RST}"

echo -e ""

echo -e ""

fi
