#!/bin/bash
# Copyright (C) 2015 Paranoid Android Project# Copyright (C) 2018 lukatthisman<shaikhlukman01@gmail.com
# Copyright (C) 2018 lukmanshaikh
 
# PA Colors
# red = errors, cyan = warnings, green = confirmations, blue = informational
# plain for generic text, bold for titles, reset flag at each end of line
# plain blue should not be used for readability reasons - use plain cyan instead
CLR_RST=$(tput sgr0)                        ## reset flag
CLR_RED=$CLR_RST$(tput setaf 1)             #  red, plain
CLR_GRN=$CLR_RST$(tput setaf 2)             #  green, plain
CLR_YLW=$CLR_RST$(tput setaf 3)             #  yellow, plain
CLR_BLU=$CLR_RST$(tput setaf 4)             #  blue, plain
CLR_PPL=$CLR_RST$(tput setaf 5)             #  purple,plain
CLR_CYA=$CLR_RST$(tput setaf 6)             #  cyan, plain
CLR_BLD=$(tput bold)                        ## bold flag
CLR_BLD_RED=$CLR_RST$CLR_BLD$(tput setaf 1) #  red, bold
CLR_BLD_GRN=$CLR_RST$CLR_BLD$(tput setaf 2) #  green, bold
CLR_BLD_YLW=$CLR_RST$CLR_BLD$(tput setaf 3) #  yellow, bold
CLR_BLD_BLU=$CLR_RST$CLR_BLD$(tput setaf 4) #  blue, bold
CLR_BLD_PPL=$CLR_RST$CLR_BLD$(tput setaf 5) #  purple, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold

echo -e "${CLR_BLD_GRN}Clone full repos...${CLR_RST}"
repo sync
echo -e "${CLR_BLD_GRN}Cloning completed...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Remove repos...${CLR_RST}"
rm -rf prebuilts/clang/host/linux-x86
rm -rf vendor/omni
rm -rf build/make
rm -rf hardware/qcom/media
rm -rf hardware/qcom/audio
rm -rf hardware/qcom/display
echo -e "${CLR_BLD_GRN}Remove repos done...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Clone repos from Omni4land...${CLR_RST}"
git clone https://github.com/Omni4land/android_kernel_xiaomi_msm8937.git -b android-8.1 kernel/xiaomi/msm8937
git clone https://github.com/Omni4land/packages_apps_Snap.git -b android-8.1 packages/apps/Snap
git clone https://github.com/Omni4land/android_device_xiaomi_land.git -b android-8.1 device/xiaomi/land
git clone https://github.com/Omni4land/prebuilts_clang_host_linux-x86.git -b android-8.1 prebuilts/clang/host/linux-x86
git clone https://github.com/Omni4land/vendor_omni.git -b android-8.1 vendor/omni
git clone https://github.com/Omni4land/build_make.git -b android-8.1 build/make
git clone https://github.com/Omni4land/external_json-c.git -b android-8.1 external/json-c
git clone https://github.com/Omni4land/vendor_xiaomi.git -b android-8.1 vendor/xiaomi
echo -e "${CLR_BLD_GRN}Clone repos from Omni4land Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Cloning Hals...${CLR_RST}"
git clone https://github.com/TeamReloaded/hardware_qcom_audio.git -b lineage-15.1-caf-8996 hardware/qcom/audio-caf/msm8996
git clone https://github.com/TeamReloaded/hardware_qcom_display.git -b lineage-15.1-caf-8996 hardware/qcom/display-caf/msm8996
git clone https://github.com/TeamReloaded/hardware_qcom_media.git -b lineage-15.1-caf-8996 hardware/qcom/media-caf/msm8996
echo -e "${CLR_BLD_GRN}Cloning Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Bionic changes...${CLR_RST}"
cd bionic && git fetch https://github.com/TeamReloaded/bionic && git cherry-pick 1e55ab48ba3f3db809163e0476051aad718d70f2^..678dd8ad22225dfef8d76a2e9cac92dc3a843e88 && cd ..
echo -e "${CLR_BLD_GRN}Bionic Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Build setup...${CLR_RST}"
. build/envsetup.sh && lunch omni_land-userdebug && brunch land
echo -e "${CLR_BLD_GRN}Build Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Now You are good to Go${CLR_RST}"
echo -e ""
echo -e ""