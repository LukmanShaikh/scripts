#first and most important stuff for setting up 
sudo apt-get update

#Compiling tools
sudo apt-get install bc bison build-essential curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev lib32readline6-dev lib32z1-dev libesd0-dev liblz4-tool libncurses5-dev libsdl1.2-dev libwxgtk3.0-dev libxml2 libxml2-utils lzop pngcrush schedtool squashfs-tools xsltproc zip zlib1g-dev ccache gcc-4.9

#Java
sudo apt-get install openjdk-8-jdk

#create bin folder
mkdir ~/bin

#set path
PATH=~/bin:$PATH

echo "export PATH=~/bin:$PATH" » ~/.bashrc

#Download repo tool
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo

#Make Repo executable
chmod a+x ~/bin/repo

#copy these in ~/.bashrc
echo "export PATH=~/bin:$PATH" » ~/.bashrc
echo "export KBUILD_BUILD_USER=Lukman" » ~/.bashrc
echo "export KBUILD_BUILD_HOST=unique-server" » ~/.bashrc

