#!/bin/bash
# Copyright (C) 2015 Paranoid Android Project# Copyright (C) 2018 lukatthisman<shaikhlukman01@gmail.com
# Copyright (C) 2018 lukmanshaikh
 
# PA Colors
# red = errors, cyan = warnings, green = confirmations, blue = informational
# plain for generic text, bold for titles, reset flag at each end of line
# plain blue should not be used for readability reasons - use plain cyan instead
CLR_RST=$(tput sgr0)                        ## reset flag
CLR_RED=$CLR_RST$(tput setaf 1)             #  red, plain
CLR_GRN=$CLR_RST$(tput setaf 2)             #  green, plain
CLR_YLW=$CLR_RST$(tput setaf 3)             #  yellow, plain
CLR_BLU=$CLR_RST$(tput setaf 4)             #  blue, plain
CLR_PPL=$CLR_RST$(tput setaf 5)             #  purple,plain
CLR_CYA=$CLR_RST$(tput setaf 6)             #  cyan, plain
CLR_BLD=$(tput bold)                        ## bold flag
CLR_BLD_RED=$CLR_RST$CLR_BLD$(tput setaf 1) #  red, bold
CLR_BLD_GRN=$CLR_RST$CLR_BLD$(tput setaf 2) #  green, bold
CLR_BLD_YLW=$CLR_RST$CLR_BLD$(tput setaf 3) #  yellow, bold
CLR_BLD_BLU=$CLR_RST$CLR_BLD$(tput setaf 4) #  blue, bold
CLR_BLD_PPL=$CLR_RST$CLR_BLD$(tput setaf 5) #  purple, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
CLR_BLD_CYA=$CLR_RST$CLR_BLD$(tput setaf 6) #  cyan, bold
echo -e ""
echo -e "${CLR_BLD_GRN}##       ##     ## ##    ## ##     ##    ###    ##    ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##   ##  ###   ###   ## ##   ###   ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##  ##   #### ####  ##   ##  ####  ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## #####    ## ### ## ##     ## ## ## ##${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##  ##   ##     ## ######### ##  ####${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}##       ##     ## ##   ##  ##     ## ##     ## ##   ###${CLR_BLD_GRN}" 
echo -e "${CLR_BLD_GRN}########  #######  ##    ## ##     ## ##     ## ##    ##${CLR_BLD_GRN}" 
echo -e ""
echo -e "${CLR_BLD_GRN}Cloning device repos...${CLR_RST}"
rm -rf device/xiaomi/land
rm -rf kernel/xiaomi/msm8937
rm -rf vendor/xiaomi
git clone https://github.com/lukmanshaikh/mdroid_device_xiaomi_land.git -b android-8.1 device/xiaomi/land
git clone https://github.com/lukmanshaikh/kernel_xiaomi_msm8937.git -b lineage-15.1 kernel/xiaomi/msm8937
git clone https://github.com/lukmanshaikh/vendor_xiaomi_land.git -b android-8.1 vendor/xiaomi
echo -e "${CLR_BLD_GRN}Cloning Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Cloning Hals...${CLR_RST}"
rm -rf hardware/qcom/audio-caf
rm -rf hardware/qcom/media-caf
rm -rf hardware/qcom/display-caf
git clone https://github.com/Sweeto143/hardware_qcom_audio.git -b oreo hardware/qcom/audio-caf/msm8996
git clone https://github.com/Sweeto143/hardware_qcom_display.git -b oreo hardware/qcom/display-caf/msm8996
git clone https://github.com/Sweeto143/hardware_qcom_media.git -b oreo hardware/qcom/media-caf/msm8996
echo -e "${CLR_BLD_GRN}Cloning Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Bionic...${CLR_RST}"
cd bionic && git fetch https://github.com/TeamReloaded/bionic && git cherry-pick 1e55ab48ba3f3db809163e0476051aad718d70f2^..678dd8ad22225dfef8d76a2e9cac92dc3a843e88 && cd ..
echo -e "${CLR_BLD_GRN}Bionic Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Build setup...${CLR_RST}"
. build/envsetup.sh && lunch mdroid_land-userdebug && mka bacon
echo -e "${CLR_BLD_GRN}Build Complete...${CLR_RST}"
echo -e ""
echo -e "${CLR_BLD_GRN}Now You are good to Go${CLR_RST}"
echo -e ""
echo -e ""